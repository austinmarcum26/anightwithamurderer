﻿using log4net;
using log4net.Appender;
using log4net.Config;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Assets.Scripts.Services {
    public class LogService {
        private static ILog logger;
        private static readonly bool logToConsole = true;

        public static void Log(string klassName, string message, object objectToLog, string logLevel = "Info") {
            if (logger == null) { SetUpLogger(); }
            if ((logLevel == "Info" || logLevel == "Debug") && logger.IsDebugEnabled) {
                var jsonInfo = ToJson(objectToLog);
                var logInfoForJson = jsonInfo == "null" ? "" : " with Object: " + jsonInfo;
                logger.Debug(klassName + " - " + message + logInfoForJson);
                if (logToConsole) { Debug.Log(klassName + " - " + message); }
            }
            if (logLevel == "Info" && logger.IsInfoEnabled && !logger.IsDebugEnabled) {
                logger.Info(klassName + " - " + message);
                if (logToConsole) { Debug.Log(klassName + " - " + message); }
            }
            if (logLevel == "Error" && logger.IsErrorEnabled) {
                logger.Error(klassName + " - " + message);
                if (logToConsole) { Debug.LogError(klassName + " - " + message); }
            }
            if (logLevel == "Warn" && logger.IsWarnEnabled) {
                logger.Warn(klassName + " - " + message);
                if (logToConsole) { Debug.LogWarning(klassName + " - " + message); }
            }
        }

        private static string ToJson(object obj) {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }

        private static void SetUpLogger() {
            var configFile = new FileInfo("Assets\\Scripts\\Config\\LogConfig.xml");
            XmlConfigurator.Configure(configFile);
            logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            var appender = LogManager.GetRepository().GetAppenders().OfType<FileAppender>().FirstOrDefault();
            var logFile = new FileInfo("Logs\\Murder.log");
            appender.File = logFile.FullName;
            Debug.Log(logFile.FullName);
            appender.ActivateOptions();
        }
    }
}

