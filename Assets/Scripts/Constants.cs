﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts {
    class Constants {
        public const string PLAYER_PREFIX = "Player ";
        public const string MURDERER_ROLE = "Murderer";
        public const string SURVIVOR_ROLE = "Survivor";
        public const int NUMBER_OF_MURDERERS_PER_SURVIVOR = 4;
    }
}
