﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Player))]
public class PlayerSetup : NetworkBehaviour
{
    [SerializeField]
    Behaviour[] componentsToDisable;
    Camera sceneCamera;

    void Start() {
        sceneCamera = Camera.main;
        if (!isLocalPlayer) {
            foreach (Behaviour behaviour in componentsToDisable) {
                behaviour.enabled = false;
            }
        } else {
            if (sceneCamera != null) {
                sceneCamera.gameObject.SetActive(false);
            }
        }
    }

    //Server Method? 
    public override void OnStartClient() {
        base.OnStartClient();
        string networkId = GetComponent<NetworkIdentity>().netId.ToString();
        Player player = GetComponent<Player>();
        GameManager.RegisterPlayer(networkId, player);
    }

    void OnDisable() {
        if (sceneCamera != null) {
            sceneCamera.gameObject.SetActive(true);
        }
        GameManager.RemovePlayer(this.transform.name);
    }
}
