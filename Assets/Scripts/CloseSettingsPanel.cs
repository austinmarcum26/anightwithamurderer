﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseSettingsPanel : MonoBehaviour
{

    public GameObject audioMenu;
    public GameObject settingsMenu;
    public GameObject mainMenu;

    public void CloseSettingsPanelOnClick()
    {
        float masterVol = audioMenu.transform.Find("Master Volume Slider").GetComponent<Slider>().value;
        float musicVol = audioMenu.transform.Find("Music Volume Slider").GetComponent<Slider>().value;
        float sfxVol = audioMenu.transform.Find("SFX Volume Slider").GetComponent<Slider>().value;

        PlayerPrefs.SetFloat("masterVolume", masterVol);
        PlayerPrefs.SetFloat("musicVolume", musicVol);
        PlayerPrefs.SetFloat("sfxVolume", sfxVol);

        audioMenu.SetActive(false);
        settingsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
}
