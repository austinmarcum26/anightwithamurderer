﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models {
    [Serializable]
    public class Message {
        public string MessageText { get; set; }
        public string From { get; set; }
        public List<string> ToList { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
