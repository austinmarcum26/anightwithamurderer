﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenSettingsPanel : MonoBehaviour
{

    public GameObject mainMenu;
    public GameObject settingsMenu;
    public GameObject audioMenu;

    public AudioManager audioManager;


   public void SettingsButtonOnClick()
    {
        audioMenu.transform.Find("Master Volume Slider").GetComponent<Slider>().value = audioManager.MasterVolume;
        audioMenu.transform.Find("Music Volume Slider").GetComponent<Slider>().value = audioManager.GetRawMusicVolume();
        audioMenu.transform.Find("SFX Volume Slider").GetComponent<Slider>().value = audioManager.GetRawSFXVolume();

        mainMenu.SetActive(false);
        settingsMenu.SetActive(true);
        audioMenu.SetActive(true);
    }
}
