using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static Dictionary<string, Player> players = new Dictionary<string, Player>();

    void Start()
    {
        DontDestroyOnLoad(gameObject);

        float masterVol = PlayerPrefs.GetFloat("masterVolume", 100);
        float musicVol = PlayerPrefs.GetFloat("musicVolume", 100);
        float sfxVol = PlayerPrefs.GetFloat("sfxVolume", 100);

        GetComponent<AudioManager>().MasterVolume = masterVol;
        GetComponent<AudioManager>().MusicVolume = musicVol;
        GetComponent<AudioManager>().SFXVolume = sfxVol;
    }
    
    public static void RegisterPlayer(string netId, Player player) {
        string playerId = Constants.PLAYER_PREFIX + netId;
        players.Add(playerId, player);
        player.transform.name = playerId;
    }

    public static void RemovePlayer(string playerId) {
        if (players.ContainsKey(playerId)) {
            players.Remove(playerId);
        }
    }

    public static List<Player> RetrieveListOfPlayers(List<string> playerNameList) {
        var playerList = new List<Player>();
        foreach (string playerName in playerNameList) {
            if (players.ContainsKey(playerName)) {
                players.TryGetValue(playerName, out Player currentPlayer);
                playerList.Add(currentPlayer);
            }
        }
        return playerList;
    }

    public static void BeginPhaseZero() {
        List<Player> playerList = players.Values.ToList();
        var numberOfMurders = Math.Ceiling(Convert.ToDecimal(playerList.Count) / Constants.NUMBER_OF_MURDERERS_PER_SURVIVOR);
        Debug.Log("Number Of Murderers: " + numberOfMurders.ToString());
        var numberOfCurrentMurderers = 0;
        foreach (Player player in playerList.OrderBy(x => Guid.NewGuid())) {
            var isMurderer = numberOfCurrentMurderers < numberOfMurders;
            player.RpcSetRole(isMurderer ? Constants.MURDERER_ROLE : Constants.SURVIVOR_ROLE);
            numberOfCurrentMurderers = numberOfCurrentMurderers + 1;
        }
    }
}
