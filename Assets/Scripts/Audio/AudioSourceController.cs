﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioSourceController : MonoBehaviour
{

    private AudioSource audioSource;
    public SoundType soundType;
    public bool playOnAwake;
    private AudioManager audioManager;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioManager = FindObjectOfType<AudioManager>();
        SetAudioVolume();

        if (playOnAwake)
        {
            audioSource.Play();
        }
    }

    void OnGUI()
    {
        SetAudioVolume();
    }

    public void Play()
    {
        SetAudioVolume();
        audioSource.Play();
    }

    private void SetAudioVolume()
    {
        audioSource.volume = audioManager.GetVolume(soundType);
    }
}
