﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public float MasterVolume { get; set; }

    private float _musicVolume;
    public float MusicVolume
    {
        get
        {
            return MasterVolume * _musicVolume;
        }

        set
        {
            _musicVolume = value;
        }
    }

    private float _sfxVolume;
    public float SFXVolume
    {
        get
        {
            return MasterVolume * _sfxVolume;
        }

        set
        {
            _sfxVolume = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float GetVolume(SoundType type)
    {
        switch(type)
        {
            case SoundType.Music:
                return MusicVolume;
            case SoundType.SFX:
                return SFXVolume;
            default:
                return MasterVolume;
        }
    }

    public float GetRawMusicVolume() { return _musicVolume; }

    public float GetRawSFXVolume() { return _sfxVolume; }
}
