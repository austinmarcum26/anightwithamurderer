﻿using Assets.Scripts.Managers;
using Assets.Scripts.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Messenger : MonoBehaviour
{
    public GameObject chatPanel;
    public GameObject textDisplay;
    public InputField textInput;

    public void DisplayMessage(Message message) {
        GameObject messageDisplay = Instantiate(textDisplay, chatPanel.transform);
        messageDisplay.GetComponent<Text>().text = message.MessageText;
    }

    void Update() {
        if (Input.GetKey(KeyCode.Return) && textInput.IsActive() && textInput.text != "") {
            var message = new Message {
                MessageText = textInput.text,
                From = transform.name,
                ToList = new List<string> { "Player 2", "Player 1" },
                CreateDate = DateTime.Now
            };
            MessagingManager.SendMessage(message);
            textInput.text = "";
            RemoveFocusToChat();
        }
    }

    public void SetFocusToChat() {
        textInput.ActivateInputField();
    }

    public void RemoveFocusToChat() {
        GetComponentInParent<Player>().SetIsInChat(false);
        textInput.DeactivateInputField();
    }
}
