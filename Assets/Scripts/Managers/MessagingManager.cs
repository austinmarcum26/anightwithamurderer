﻿using Assets.Scripts.Models;
using Assets.Scripts.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Managers {

    class MessagingManager: NetworkBehaviour {

        public static List<Message> messages = new List<Message>();

        public static void SendMessage(Message message) {
            messages.Add(message);
            List<Player> players = GameManager.RetrieveListOfPlayers(message.ToList);
            foreach (Player player in players) {
                var messageByteArray = ServerCommunicationService.SerializeToByteArray(message);
                player.RpcGetMessage(messageByteArray);
            }
        }

        public static List<Message> RetrieveMessagesForPlayer(string playerName) {
            return messages.Where(x => x.ToList.Contains(playerName)).ToList();
        }

        public static List<Message> RetrieveAllMessages() {
            return messages;
        }
    }
}
