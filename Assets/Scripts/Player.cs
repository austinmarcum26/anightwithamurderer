﻿using Assets.Scripts.Managers;
using Assets.Scripts.Models;
using Assets.Scripts.Services;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour {

    public string Role { get; set; }
    public bool IsInChat { get; set; }

    void Update() {
        //Until the systems are working, this is where we can test different functionalities of the game
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            CmdBeginPhaseZero();
        }
        if (Input.GetKeyDown(KeyCode.T)) {
            IsInChat = true;
            GetComponentInChildren<Messenger>().SetFocusToChat();
        }
    }

    [Command]
    public void CmdBeginPhaseZero() {
        GameManager.BeginPhaseZero();
    }

    [Command]
    public void CmdSendMessage() {
        MessagingManager.SendMessage(new Message {
            MessageText = "Hello",
            From = transform.name,
            ToList = new List<string> { "Player 2", "Player 1" },
            CreateDate = DateTime.Now
        });
    }

    [ClientRpc]
    public void RpcGetMessage(byte[] messageByteArray) {
        if (isLocalPlayer) {
            Message message = ServerCommunicationService.Deserialize<Message>(messageByteArray);
            GetComponentInChildren<Messenger>().DisplayMessage(message);
        }
    }

    [ClientRpc]
    public void RpcSetRole(string role) {
        if (isLocalPlayer) {
            Role = role;
            Debug.Log(transform.name + " has role " + Role);
        }
    }

    public void SetIsInChat(bool isInChat) { IsInChat = isInChat;  }
    public bool GetIsInChat() { return IsInChat;  }
}
